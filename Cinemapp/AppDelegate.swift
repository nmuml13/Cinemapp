//
//  AppDelegate.swift
//  Cinemapp
//
//  Created by nini on 19/01/17.
//  Copyright © 2017 nini. All rights reserved.
//

import UIKit
import UserNotifications


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert,  .sound, .badge], completionHandler:
            {
            granted, error in
                //handle error
                if granted {
                     
                }
        })
        
//        var types: UIUserNotificationType = UIUserNotificationType()
//        types.insert(UIUserNotificationType.alert)
//        types.insert(UIUserNotificationType.badge)
//        
//        let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert,  .sound, .badge], categories: nil)
//        
//        UIApplication.shared.registerUserNotificationSettings(settings)
        
        // Override point for customization after application launch.
        // Initialize sign-in
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        GIDSignIn.sharedInstance().delegate = self
        
        
        /* check for user's token */
        let model = DatabaseProxy.shared()
        let _ = model.downloadCurrentMovies()
        
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if GIDSignIn.sharedInstance().currentUser != nil {
            /* Code to show your tab bar controller */
            print("user is signed in")
            
            if let tabBarVC = sb.instantiateViewController(withIdentifier: "TabController") as? UITabBarController {
                appDelegate.window!.rootViewController = tabBarVC
            }
        } else {
            print("user is NOT signed in")
            /* code to show your login VC */
            if let tabBarVC = sb.instantiateViewController(withIdentifier: "ViewController") as? ViewController {
                
                appDelegate.window!.rootViewController = tabBarVC
            }
        }
        

        return true
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if( error != nil){
            print("we've got an error sign in \(error)")
        } else {
            print("Sign in successful")
            let sb = UIStoryboard(name: "Main", bundle: nil)
            if let tabBarVC = sb.instantiateViewController(withIdentifier: "TabController") as? UITabBarController {
                window!.rootViewController = tabBarVC
//                tabBarVC.tabBar.tintColor = UIColor.gray
                DatabaseProxy.shared().addNewUser(userName: user.userID, password: "", email: user.profile.email)
                DatabaseProxy.shared().setCurrentUser(u: user.userID)
            }

        }
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url,
                                                    sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                    annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    }
   

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

