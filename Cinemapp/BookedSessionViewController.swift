//
//  BookedSessionViewController.swift
//  Cinemapp
//
//  Created by nini on 29/01/17.
//  Copyright © 2017 nini. All rights reserved.
//

import UIKit

class BookedSessionViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBAction func close(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)

    }
   
    
    @IBOutlet weak var seatsTable: UITableView!
    var currentSession : Session?
    var seatsInfo: Array<(section: Int, seat: Int)>?
    @IBOutlet weak var day: UILabel!


    @IBOutlet weak var hall: UILabel!
    @IBOutlet weak var startTime: UILabel!
    

    
    func getSeatsInString() -> String {
        var text = " Your Seats: "
        let sorted = seatsInfo?.sorted(by: { $0.section < $1.section})
        
        var index = 0
        while (index < (sorted?.count)!) {
            let stringRow: String = String(describing: (sorted?[index].section)!) as String
            text += " Row: " + stringRow
            
            while true {
                let stingSeat: String = String(describing: (sorted?[index].seat)!)
                text += " Seat :" + stingSeat
                index += 1
                if false == (index < (sorted?.count)! && (sorted?[index - 1].section)! == (sorted?[index].section)!) {
                    text += "\n"
                    break
                }
            }
        }
        return text
    }
    
    @IBOutlet weak var popview: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        popview.layer.cornerRadius = 10
        popview.layer.masksToBounds = true
        
        seatsTable.layer.cornerRadius = 10
        let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.short
        formatter.timeStyle = DateFormatter.Style.none
        
        let dateString = formatter.string(from: (currentSession?.day)!)
        day.text! = dateString
        startTime.text! = (currentSession?.startTime)!
        let stringHall: String = String(describing: (currentSession?.hall.num)!)
        hall.text! = "hall: " + stringHall
//        print(getSeatsInString())    seats.text! = getSeatsInString()
        
//        startTime.text = (currentSession?.startTime)!
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return seatsInfo!.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "seatInfoCell", for: indexPath)
        let row: String = String(describing: ((seatsInfo?[indexPath.row])?.0)!)
        let seat: String = String(describing: ((seatsInfo?[indexPath.row])?.1)!)
        cell.textLabel?.text = "Row: " + row
        cell.detailTextLabel?.text = "Seat: " + seat
        return cell
        
    }


    /*
     MARK: - Navigation

     In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     
         Get the new view controller using segue.destinationViewController.
         Pass the selected object to the new view controller.
    }
    */

}
