//
//  MovieViewController.swift
//  Cinemapp
//
//  Created by nini on 25/01/17.
//  Copyright © 2017 nini. All rights reserved.
//

import UIKit

class MovieViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var model = DatabaseProxy.shared()
    var movieToDisplay: Movie?
    var sessionsToDisplay: Array<Array<Session>>?
    
    
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var poster: UIImageView!

    @IBOutlet weak var youtubeButton: UIButton!
    @IBOutlet weak var sessionTable: UITableView!
    
    @IBOutlet weak var imdbButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nameString: String = (movieToDisplay?.name)!
        name.text! = nameString
        sessionTable.layer.cornerRadius = 10
        
        let ratingString: String = String(describing: (movieToDisplay?.rating)!)
        rating.text! = ratingString
        
        fetchImage(img: (movieToDisplay?.poster)!)
        sessionsToDisplay = model.getSessionsForMovie(forMovie: movieToDisplay!)

        // Do any additional setup after loading the view.
    }
    
    
    private func fetchImage(img: URL){
        let dQueue = DispatchQueue(label: "com.app.queue",
                                            qos: .default,
                                            target: nil)
        dQueue.async {
            let imageData = try? Data(contentsOf: img)
            
            if imageData != nil {
                let image = UIImage(data: imageData!)
                DispatchQueue.main.async(execute: { () -> Void in
                    self.poster.image = image
                })
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return sessionsToDisplay![section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sessionsToDisplay!.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let day = sessionsToDisplay?[section][0].day
        let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.long
        formatter.timeStyle = DateFormatter.Style.none
        
        let dateString = formatter.string(from: day!)
        return dateString
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sessionCell", for: indexPath)
        let session = sessionsToDisplay?[indexPath.section][indexPath.row]
        
        let time = session?.startTime
//        let day = sessionToDisplay.day
//        let formatter = DateFormatter()
//        formatter.dateStyle = DateFormatter.Style.long
//        formatter.timeStyle = DateFormatter.Style.medium
//        
//        let dateString = formatter.string(from: day)
        cell.textLabel?.text! = time!
        cell.detailTextLabel?.text! = (session?.language.rawValue)!
        return cell

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
     MARK: - Navigation

     In a storyboard-based application, you will often want to do a little preparation before navigation
   
    */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        Get the new view controller using segue.destinationViewController.
//        Pass the selected object to the new view controller.
        if segue.identifier == "showTrailer" {
            if let destVC = segue.destination as? YoutubeTrailerViewController {
                destVC.movieToDisplay = self.movieToDisplay
            }

        } else if segue.identifier == "showSession" {
//            let navController = segue.destination as? UINavigationController
            let destVC = segue.destination as! SessionViewController
                if let cell = sender as? UITableViewCell, let indexPath = sessionTable.indexPath(for: cell) {
                    destVC.sessionToDisplay = sessionsToDisplay?[indexPath.section][indexPath.row]
                    destVC.movieToDisplay = self.movieToDisplay
                }
            
        }
        
    }
}
