//
//  RegistrationViewController.swift
//  Cinemapp
//
//  Created by nini on 24/01/17.
//  Copyright © 2017 nini. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController {
    @IBOutlet weak var userName: UITextField!
    
    @IBOutlet weak var mail: UITextField!

    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var repeatPassword: UITextField!
    
    var model: DatabaseProxy?
    
    @IBAction func registerNewUser(_ sender: Any) {
        if(!model!.contains(userName: userName.text!) && passwordsMatch() && emailIsValid()){
            model!.addNewUser(userName: userName.text!, password: password.text!, email: mail.text!)
        }
        let sb = UIStoryboard(name: "Main", bundle: nil)
        if let tabBarVC = sb.instantiateViewController(withIdentifier: "TabController") as? UITabBarController {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.rootViewController = tabBarVC
        }
         model?.setCurrentUser(u: userName.text!)
    }
    
    func passwordsMatch() -> Bool {
        if(password.text! == repeatPassword.text){
            return true
        }
        return false
    }
    
    func emailIsValid() -> Bool {
        if(mail.text!.contains("@") && mail.text!.contains(".")){
            return true
    }
        return false
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
