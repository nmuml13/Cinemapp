//
//  Session.swift
//  Cinemapp
//
//  Created by nini on 24/01/17.
//  Copyright © 2017 nini. All rights reserved.
//

import Foundation

struct Session {
    var movie: String
    var startTime: String
    var day: Date
    var language: languageOption
    var cinema: String
    var hall: Hall
    var price: String
    
    
    static func == (lhs: Session, rhs: Session) -> Bool {
        return lhs.movie == rhs.movie && lhs.startTime == rhs.startTime
    }
}

enum languageOption: String {
    case English
    case Georgian
    case Russian
}

struct Hall {
    var numberOfRows: Int
    var numberOfSeats: Int
    var num: Int
    var seats : Array<Array <Bool>>
    
    init(numberOfRows: Int, numberOfSeats: Int, num: Int) {
        self.numberOfRows = numberOfRows
        self.numberOfSeats = numberOfSeats
        self.num = num
        self.seats = Array(repeating:Array(repeating:false, count:numberOfSeats), count: numberOfRows)
    }
    
    
    
}
