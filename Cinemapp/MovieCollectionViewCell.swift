//
//  MovieCollectionViewCell.swift
//  Cinemapp
//
//  Created by nini on 30/01/17.
//  Copyright © 2017 nini. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var heartButton: UIButton!
    @IBOutlet weak var name: UILabel!
    
    var movie: Movie?
    
    func setUp(){
//        self.selectionStyle = .none
        self.name.text! = (movie?.name)!
        let backgroundQueue = DispatchQueue(label: "com.app.queue",
                                            qos: .default,
                                            target: nil)
        
        backgroundQueue.async {
            let imageData = try? Data(contentsOf: (self.movie?.poster)!)
            //            let image = UIImage(data: imageData!)
            //            cell.imageView?.image = image
            
            
            if imageData != nil {
                let image = UIImage(data: imageData!)
                DispatchQueue.main.async(execute: { () -> Void in
                    self.image?.image = image
                    self.setNeedsLayout()
                })
            }
        }
//        self.director.text = "Director:" + (movie?.director)!
//        self.cast.text = "Cast:" + (movie?.cast)!
//        
//        let ratingString: String = String(describing: (movie!.rating))
//        self.rating.text! = "IMDB rating:" + ratingString
        
        
    }

}
