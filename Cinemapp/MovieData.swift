//
//  MovieData.swift
//  Cinemapp
//
//  Created by nini on 30/01/17.
//  Copyright © 2017 nini. All rights reserved.
//

import Foundation

class MovieData {
    private static var downloadedData: NSDictionary = NSDictionary()
    public static var searchString = ""
    
    public static func saveDownloadedData(_ data: Data){
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! NSDictionary
            downloadedData = json
        } catch {
            
        }
        
        
    }
    
    public static func getCount() -> Int {
        return downloadedData.count
    }
    
    public static func getInfo() -> NSDictionary{
        return downloadedData
    }
}
