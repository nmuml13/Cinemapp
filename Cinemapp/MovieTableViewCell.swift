//
//  MovieTableViewCell.swift
//  Cinemapp
//
//  Created by nini on 25/01/17.
//  Copyright © 2017 nini. All rights reserved.
//

import UIKit

class MovieTableViewCell: UITableViewCell {
   
    @IBOutlet weak var director: UILabel!
    @IBOutlet weak var cast: UILabel!

    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var poster: UIImageView!
    
    @IBOutlet weak var heart: UIButton!
    @IBOutlet weak var title: UILabel!
    
 
     
    var movie: Movie?
    
    func setUp(){
        self.selectionStyle = .none
        self.title.text! = (movie?.name)!
        let backgroundQueue = DispatchQueue(label: "com.app.queue",
                                            qos: .default,
                                            target: nil)
        
        backgroundQueue.async {
            let imageData = try? Data(contentsOf: (self.movie?.poster)!)
            //            let image = UIImage(data: imageData!)
            //            cell.imageView?.image = image
            
            
            if imageData != nil {
                let image = UIImage(data: imageData!)
                DispatchQueue.main.async(execute: { () -> Void in
                    self.poster?.image = image
                    self.setNeedsLayout()
                })
            }
        }
        self.director.text = "Director:" + (movie?.director)!
        self.cast.text = "Cast:" + (movie?.cast)!
        
        let ratingString: String = String(describing: (movie!.rating))
        self.rating.text! = "IMDB rating:" + ratingString


    }

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        print(movie ?? " nulll")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
