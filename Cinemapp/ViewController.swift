//
//  ViewController.swift
//  Cinemapp
//
//  Created by nini on 23/01/17.
//  Copyright © 2017 nini. All rights reserved.
//

import UIKit

class ViewController: UIViewController, GIDSignInUIDelegate  {
    @IBOutlet weak var signInButton: UIButton!

    @IBOutlet weak var GoogleSignIn: GIDSignInButton!
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    var model = DatabaseProxy.shared()
    
    @IBAction func SignInClicked(_ sender: UIButton) {
        if(model.isRightUser(userName: userName.text!, password: password.text!)){
            model.setCurrentUser(u: userName.text!)
            let sb = UIStoryboard(name: "Main", bundle: nil)
            if let tabBarVC = sb.instantiateViewController(withIdentifier: "TabController") as? UITabBarController {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window!.rootViewController = tabBarVC
            }

        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        signInButton.layer.cornerRadius = 5
        signInButton.layer.masksToBounds = true
        registerButton.layer.cornerRadius = 5
        registerButton.layer.masksToBounds = true
        userName.layer.borderWidth = 1
        userName.layer.cornerRadius = 5
        userName.layer.masksToBounds = true
        userName.layer.borderColor = UIColor.gray.cgColor
        password.layer.borderWidth = 1
        password.layer.cornerRadius = 5
        password.layer.masksToBounds = true

        password.layer.borderColor = UIColor.gray.cgColor
        GoogleSignIn.style = GIDSignInButtonStyle.iconOnly
        GIDSignIn.sharedInstance().uiDelegate = self
        
        // Uncomment to automatically sign in the user.
        // GIDSignIn.sharedInstance().signInSilently()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
     MARK: - Navigation
     In a storyboard-based application, you will often want to do a little preparation before navigation
    */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Registration" {
            if let destVC = segue.destination as? RegistrationViewController {
                destVC.model = self.model
            }
        }

        
        
    }


}
