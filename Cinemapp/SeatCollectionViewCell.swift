//
//  SeatCollectionViewCell.swift
//  Cinemapp
//
//  Created by nini on 26/01/17.
//  Copyright © 2017 nini. All rights reserved.
//

import UIKit

class SeatCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var numSeat: UILabel!
    var pressed : Bool = false
    
    
    override func awakeFromNib() {
        self.contentView.layer.cornerRadius = 10
        self.contentView.layer.masksToBounds = true
        pressed = false
    }

    
}
