//
//  WatchListViewController.swift
//  Cinemapp
//
//  Created by nini on 26/01/17.
//  Copyright © 2017 nini. All rights reserved.
//

import UIKit

class WatchListViewController: UITableViewController {
    
    var model: DatabaseProxy = DatabaseProxy.shared()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    @IBAction func displayInfo(_ sender: UIButton) {
        
        let touchPoint = sender.convert(CGPoint.zero, to:tableView)
        
        let clickedButtonIndexPath = tableView.indexPathForRow(at: touchPoint)
        let cell = tableView.cellForRow(at: clickedButtonIndexPath!) as! WatchlistTableViewCell
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "BookedSessionViewController") as! BookedSessionViewController
        
        for info in model.getUsersWatchList(userName: model.getCurrentUser().userName){
            if info.0 == cell.bookedMovie!{
                
                if let session = info.1{
                    if session == cell.bookedSession! {
                        secondViewController.seatsInfo = info.2
                        secondViewController.currentSession = info.1
                    }
                }
                
            }
        }
        self.present(secondViewController, animated: true, completion: nil)
        
    }
    

    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return  model.getUsersWatchList(userName: model.getCurrentUser().userName).count

    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "watchListCell", for: indexPath) as? WatchlistTableViewCell

//         Configure the cell...
        let watchList = model.getUsersWatchList(userName: model.getCurrentUser().userName)
        cell?.title.text = watchList[indexPath.row].0.name
        cell?.selectionStyle = .none
        let backgroundQueue = DispatchQueue(label: "com.app.queue",
                                            qos: .default,
                                            target: nil)
        
        backgroundQueue.async {
            let imageData = try? Data(contentsOf: watchList[indexPath.row].0.poster)
                if imageData != nil {
                    let image = UIImage(data: imageData!)
                    DispatchQueue.main.async(execute: { () -> Void in
                        cell?.movieImage.image = image
                        cell?.setNeedsLayout()
                })
            }
        }

      
        if  let session = watchList[indexPath.row].1{
            cell?.seeInfo.isHidden = false
            cell?.bookedSession = session
            cell?.bookedMovie = watchList[indexPath.row].0
        }
        return cell!
    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
