//
//  JSONDownloader.swift
//  Cinemapp
//
//  Created by nini on 30/01/17.
//  Copyright © 2017 nini. All rights reserved.
//

import Foundation


class JSONDownloader {
    
    private static let defaultSession = URLSession(configuration: URLSessionConfiguration.default)
    private static var task: URLSessionDataTask?
    
    static func download(_ url: String, _ downloadCallBack: @escaping (_ saveDownloadedData: Data) -> Void){
        
        if task != nil {
            task?.cancel()
        }
        let replaced = String(url.characters.map {
            $0 == " " ? "+" : $0
        })
        let urlString = "http://www.omdbapi.com/?t=" + replaced + "&y=&plot=full&r=json"
        let url = URL(string: urlString)!
        let request = URLRequest(url: url)
        task = defaultSession.dataTask(with: request) {
            data, response, error in
            if let error = error {
                print(error.localizedDescription)
            } else if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200{
                downloadCallBack(data!)
            }
        }
        task?.resume()
 
    }

   
}
