//
//  User.swift
//  Cinemapp
//
//  Created by nini on 24/01/17.
//  Copyright © 2017 nini. All rights reserved.
//

import Foundation

struct User {
    var userName: String
    var password: String
    var email: String
    var watchList: Array <(Movie, Session?, Array<(section: Int, seat: Int)>)>
    var toGoList: Array <Session>
}
