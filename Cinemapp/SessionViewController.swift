//
//  SessionViewController.swift
//  Cinemapp
//
//  Created by nini on 25/01/17.
//  Copyright © 2017 nini. All rights reserved.
//

import UIKit
import UserNotifications


class SessionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var model = DatabaseProxy.shared()
    
    @IBOutlet weak var startTime: UILabel!
    @IBOutlet weak var hallNumber: UILabel!
   
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var language: UILabel!
    @IBOutlet weak var seats: UICollectionView!
    
    @IBOutlet weak var price: UILabel!
    var sessionToDisplay: Session?
    var movieToDisplay: Movie?
    
    var hallSeats : Array<Array <Bool>>?
    var bookedSeats: Array<(section: Int, seat: Int)> = []
    
    var timeInterval: String = UserDefaults().string(forKey: "timeInterval") ?? " "
   
    func scheduleNotification(){
//        let hoursBefore = Int(timeInterval!)
        
        let content = UNMutableNotificationContent()
        content.title = "tickets booked"
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        let requestID = "reminder"
        let request = UNNotificationRequest(identifier: requestID, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request , withCompletionHandler: nil)
//        let trigger = UN
    }
    
    
    @IBAction func bookSeats(_ sender: UIButton) {
        model.addToUsersWatchList(userName: model.getCurrentUser().userName, toAdd: (a: movieToDisplay!, b: sessionToDisplay,c:bookedSeats))
        
        model.updateSeats(movieToDisplay: movieToDisplay!, sessionToDisplay: sessionToDisplay!, hallSeats: hallSeats!)
        
        scheduleNotification()
        seats.reloadData()
    }
    
    func changedTimeInterval(){
        print("previous value\(timeInterval)")
        timeInterval = UserDefaults().string(forKey: "timeInterval")!
        print("new value\(timeInterval)")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
//        UNUserNotificationCenter.current().delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector (SessionViewController.changedTimeInterval), name: UserDefaults.didChangeNotification, object: nil)
        
        seats.dataSource = self
        seats.delegate = self
        seats.allowsMultipleSelection = true
        startTime.text! =  (sessionToDisplay?.startTime)!
        hallNumber.text! = "Hall Number: " + String(sessionToDisplay!.hall.num)
        movieTitle.text! = (sessionToDisplay?.movie)!
        language.text! = (sessionToDisplay?.language.rawValue)!
        price.text! = (sessionToDisplay?.price)!
        hallSeats = model.getSeats(movieToDisplay: movieToDisplay!, sessionToDisplay: sessionToDisplay!)
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (sessionToDisplay?.hall.numberOfSeats)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = seats.dequeueReusableCell(withReuseIdentifier: "seatCell", for: indexPath) as? SeatCollectionViewCell
        cell?.numSeat.text = String(indexPath.row + 1)
        cell?.numSeat.backgroundColor = UIColor.black
        cell?.numSeat.textColor = UIColor.white
        if(hallSeats?[indexPath.section][indexPath.row] == true){
            cell?.numSeat.backgroundColor = UIColor.lightGray
        }
        return cell!
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return (sessionToDisplay?.hall.numberOfRows)!
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let numSeats = collectionView.numberOfItems(inSection: indexPath.section)
        let width = CGFloat(collectionView.bounds.size.width.divided(by: CGFloat(numSeats)))
        
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = seats.cellForItem(at: indexPath )as? SeatCollectionViewCell
        var taken = false
        for (_, value) in bookedSeats.enumerated(){
            if value.0 == indexPath.section && value.1 == indexPath.row {
                taken = true
            }
        }
        if(taken == false){
            cell?.numSeat.backgroundColor = UIColor.init(colorLiteralRed: 0.3724, green: 0.1233 , blue: 0.2516, alpha: 1)
            cell?.numSeat.textColor = UIColor.black

            
            cell?.setNeedsLayout()
            hallSeats?[indexPath.section][indexPath.row] = true
            let sect = indexPath.section + 1
            let seat = indexPath.row + 1
            bookedSeats.append(section: sect, seat: seat)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = seats.cellForItem(at: indexPath )as? SeatCollectionViewCell
        cell?.numSeat.backgroundColor = UIColor.black
        cell?.numSeat.textColor = UIColor.white
        cell?.setNeedsLayout()
        hallSeats?[indexPath.section][indexPath.row] = false
        for (index, value) in bookedSeats.enumerated(){
            if value.0 == indexPath.section && value.1 == indexPath.row {
                bookedSeats.remove(at: index)
            }
        }
        
    }
    
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return CGFloat(10)
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 5
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
