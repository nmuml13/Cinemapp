//
//  DatabaseProxy.swift
//  Cinemapp
//
//  Created by nini on 24/01/17.
//  Copyright © 2017 nini. All rights reserved.
//

import Foundation

class DatabaseProxy {
    
    private static var database = DatabaseProxy()
    
    var users : Dictionary = [String: User] ()
    public var currentUser: User?
    
    
    
    
    func contains(userName: String) -> Bool{
        let keyExists = users[userName] != nil
        return keyExists
    }
    
    func isRightUser(userName: String, password:String) -> Bool {
        if let myUser = users[userName] {
            if myUser.password == password {
                return true
            }
        }
        return false
    }
    
    func addNewUser(userName : String, password: String , email: String){
        let u = User(userName: userName, password: password, email: email, watchList: [(Movie, Session?, Array<(section: Int, seat: Int)>)](), toGoList: [Session]())
        users[userName] = u
    }

    func getUsersWatchList(userName : String) -> Array<((a: Movie, b: Session?, Array<(section: Int, seat: Int)>))> {
        if(self.contains(userName: userName)){
            let result: Array = users[userName]!.watchList
            return result
        }
        return []
    }
    
    func getUsersSessions(userName : String) -> Array<Session> {
        if(self.contains(userName: userName)){
            let result: Array = users[userName]!.toGoList
            return result
        }
        return [Session]()
    }
    
    func areEqual(rhs :(Movie, Session?), lhs: (Movie, Session?)) -> Bool{
        if let a = rhs.1 {
            if let b = lhs.1{
                if a == b{
                    return true
                }
            }
        }
        return false
    }
    
    typealias T =  (Movie, Session?, Array<(section: Int, seat: Int)>)
 
    func containsTuple(array : Array<(section: Int, seat: Int)>, mySeat : (section: Int, seat: Int)) -> Bool{
        for t in array{
            if t.0 == mySeat.0  && t.1 == mySeat.1 {
                return true
            }
        }
        return false
    }
 
    
    func addToUsersWatchList(userName: String, toAdd: (a: Movie, b: Session?, c: Array<(section: Int, seat: Int)>)){
        var newToAdd = toAdd
        var same = false
        var newSeats = false
        var toDeleatIndex = -1
        for (index, info) in ((users[userName]?.watchList)!).enumerated(){
            if info.0 == toAdd.0 {
                if let session = toAdd.1 {
                    if session.day == info.1?.day && session.startTime == info.1?.startTime {
                        var newArray = toAdd.c
                        for seat in info.2{
                            if(containsTuple(array: newArray, mySeat: seat) == false){
                                newArray.append(seat)
                            }
                            
                        }
                        newToAdd = (toAdd.a, toAdd.b, newArray)
                        newSeats = true
                        toDeleatIndex = index
                    }
                } else {
                    same = true
                }
                
            }
            

        }
        if(same == false){
            if(newSeats){
                self.users[userName]?.watchList.remove(at: toDeleatIndex)
            }
            self.users[userName]?.watchList.append(newToAdd)
        }
    }
    // returns sessions ordered according to their dates
    func getSessionsForMovie(forMovie : Movie) -> Array <Array <Session>> {
        let sorted = forMovie.sessions.sorted(by: { $0.day < $1.day})
        var result = Array <Array <Session>> ()
        var index = 0
        while index < sorted.count {
            var sessionForDay = Array<Session> ()
            sessionForDay.append(sorted[index])
            index += 1
            while  index < sorted.count && sorted[index - 1].day == sorted[index].day {
                sessionForDay.append(sorted[index])
                index += 1
            }
            
            
            result.append(sessionForDay)
        }
        
        return result
        
    }
    func getSeats(movieToDisplay: Movie, sessionToDisplay: Session) ->  Array<Array <Bool>>{
        let oldMovies = getCurrentMovies()
        for (_,movie) in oldMovies.enumerated(){
            if movie == movieToDisplay {
                for (_, session) in movie.sessions.enumerated(){
                    if sessionToDisplay == session {
                        return session.hall.seats
                    }
                }
            }
        }
        return Array<Array<Bool>>()
    }
    func updateSeats(movieToDisplay: Movie, sessionToDisplay: Session, hallSeats: Array<Array <Bool>>){
        var oldMovies = getCurrentMovies()
        
        for (movieIndex,movie) in oldMovies.enumerated(){
            if movie == movieToDisplay {
                for (sessionIndex, session) in movie.sessions.enumerated(){
                    if sessionToDisplay == session {
                        var newSession = session
                        newSession.hall.seats = hallSeats
                        var newMovie = movie
                        newMovie.sessions[sessionIndex] = newSession
                        
                        oldMovies[movieIndex] = newMovie
                    }
                }
            }
        }
        result = oldMovies
    }
    
    var result = Array<Movie>()
    
        func getInfo ( _ downloadedData: Data){
        MovieData.saveDownloadedData(downloadedData)
        let date = Date.from(year: 2017, month: 01, day: 28)
        let newdate = Date.from(year: 2017, month: 01, day: 29)
        let futuredate = Date.from(year: 2017, month: 01, day: 30)

        
        let info: NSDictionary = MovieData.getInfo()

        let title = info["Title"] as? String ?? ""
        let runtime = info["Runtime"] as? String ?? ""
        let actors = info["Actors"] as? String ??  ""
        let poster = info["Poster"] as? String ?? ""
        let director = info["Director"] as? String ?? ""
        var ratingDouble = 0.0
        if let rating = info["imdbRating"] {
            ratingDouble = Double((rating as? String)!)!
        }

        
        let sessionForMovie = [Session(movie: title,startTime: "12:30", day: date, language: languageOption.English, cinema:"Cavea", hall: Hall(numberOfRows: 5, numberOfSeats: 10, num: 3), price: "12ლ"),Session(movie: title, startTime: "16:30", day: date, language: languageOption.Russian, cinema: "Cavea", hall: Hall(numberOfRows: 5, numberOfSeats: 10, num: 2), price: "14ლ"),Session(movie: title, startTime: "14:00", day: newdate, language: languageOption.Georgian, cinema: "Cavea", hall: Hall(numberOfRows: 5, numberOfSeats: 10, num: 2), price: "16ლ"), Session(movie: title, startTime: "18:30", day: newdate, language: languageOption.Georgian, cinema: "Cavea", hall: Hall(numberOfRows: 5, numberOfSeats: 10, num: 2), price: "16ლ"), Session(movie: title, startTime: "22:30", day: newdate, language: languageOption.English, cinema: "Cavea", hall: Hall(numberOfRows: 5, numberOfSeats: 10, num: 2), price: "16ლ"),  Session(movie: title, startTime: "14:00", day: futuredate, language: languageOption.English, cinema: "Cavea", hall: Hall(numberOfRows: 5, numberOfSeats: 10, num: 2), price: "16ლ")]

        let movieToAppend = Movie(name: title, director: director, cast: actors, duration: runtime, sessions:sessionForMovie, rating: ratingDouble, poster: URL(string: poster)!, videoID: "0pdqf4P9MB8")
        result.append(movieToAppend)


    }
    
    func downloadCurrentMovies(){
        let movieNames = ["La La Land", "The Space Between Us ", "Hidden Figures", "Passengers", "Moulin Rouge", "Arrival", "Gone Girl", "Cafe Society", "The Curious Case of Benjamin Button"]
        
        for movieName in movieNames {
            let backgroundQueue = DispatchQueue(label: "com.app.queue",
                                                qos: .default,
                                                target: nil)
            backgroundQueue.async {
                
                
                JSONDownloader.download(movieName, self.getInfo)
                
            }
        }

    }

    
    func getCurrentMovies() -> Array <Movie> {
       
        return result
    }
    
    
    func setCurrentUser (u: String){
        currentUser = users[u]
    }
    
    
    func getCurrentUser() -> User {
        return (currentUser ?? nil)!
    }
    
    class func shared() -> DatabaseProxy {
        return database
    }
    
}

extension Date {
    
    static func from(year: Int, month: Int, day: Int) -> Date {
        let gregorianCalendar = NSCalendar(calendarIdentifier: .gregorian)!
        
        var dateComponents = DateComponents()
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = day
        
        let date = gregorianCalendar.date(from: dateComponents)!
        return date
    }
    
    static func parse(_ string: String, format: String = "yyyy-MM-dd") -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.default
        dateFormatter.dateFormat = format
        
        let date = dateFormatter.date(from: string)!
        return date
    }
}
