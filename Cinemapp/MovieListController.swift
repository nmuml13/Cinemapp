//
//  MovieListController.swift
//  Cinemapp
//
//  Created by nini on 24/01/17.
//  Copyright © 2017 nini. All rights reserved.
//

import UIKit

class MovieListController: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collection: UICollectionView!
   
    @IBOutlet weak var signOutButton: UIBarButtonItem!

    @IBOutlet weak var table: UITableView!
    
    var model = DatabaseProxy.shared().getCurrentMovies()
    var displayMode: Bool = UserDefaults().bool(forKey: "display_collection")
    
    
    func changedMode(){
        displayMode = UserDefaults().bool(forKey: "display_collection")
        table.isHidden = displayMode
        collection.isHidden = !displayMode
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector (MovieListController.changedMode), name: UserDefaults.didChangeNotification, object: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        signOutButton.layer.cornerRadius = 10
//        signOutButton.layer.masksToBounds = true
//        table.allowsSelection = false
        
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isTranslucent = false
        self.tabBarController?.tabBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        
        self.navigationController?.navigationBar.barTintColor =  UIColor.init(colorLiteralRed: 0.0401, green:0.1869 , blue:0.2751, alpha: 0.5)

        
        self.tabBarController?.tabBar.barTintColor = UIColor.init(colorLiteralRed: 0.0401, green:0.1869 , blue:0.2751, alpha: 0.5)
        table.isHidden = displayMode
        collection.isHidden = !displayMode
        
        
//        self.tabBarController?.tabBar.tintColor =  UIColor.init(colorLiteralRed: 0.0401, green:0.1869 , blue:0.2751, alpha: 0.5)


    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collection.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as? MovieCollectionViewCell
        cell?.movie = model[indexPath.row]
        cell?.setUp()
        return cell!
    }
    

   
    @IBAction func add(_ sender: UIButton) {
        let touchPoint = sender.convert(CGPoint.zero, to: self.collection)
        let clickedButtonIndexPath = collection.indexPathForItem(at: touchPoint)
        let cell = collection.cellForItem(at: clickedButtonIndexPath!) as? MovieCollectionViewCell
        cell?.heartButton.setTitleColor(UIColor.init(colorLiteralRed: 0.3724, green: 0.1233 , blue: 0.2516, alpha: 1), for: .normal)
        let userName = (DatabaseProxy.shared().currentUser?.userName)!
          DatabaseProxy.shared().addToUsersWatchList(userName: userName, toAdd: ((cell?.movie!)!, nil,  Array<(section: Int, seat: Int)>()))
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
    
    @IBAction func addToWatchList(_ sender: UIButton) {
        let touchPoint = sender.convert(CGPoint.zero, to: self.table)
        let clickedButtonIndexPath = table.indexPathForRow(at: touchPoint)
        let cell = table.cellForRow(at: clickedButtonIndexPath!) as! MovieTableViewCell
        cell.heart.setTitleColor(UIColor.init(colorLiteralRed: 0.3724, green: 0.1233 , blue: 0.2516, alpha: 1), for: .normal)
        let userName = (DatabaseProxy.shared().currentUser?.userName)!
        DatabaseProxy.shared().addToUsersWatchList(userName: userName, toAdd: (cell.movie!, nil,  Array<(section: Int, seat: Int)>()))
        
        
    }
    
    func showPopUpController(cell: MovieTableViewCell){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let popUpVC = sb.instantiateViewController(withIdentifier: "PopQuestionController") as? PopQuestionViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController?.present(popUpVC!, animated: true, completion: nil)
        popUpVC?.movieToDisplay = cell.movie!
        
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        

        let width = CGFloat(collectionView.bounds.size.width.divided(by: CGFloat(2)) - 5)
        
        return CGSize(width: width, height: width)
    }

    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell", for: indexPath) as! MovieTableViewCell
        let movieToDisplay = model[indexPath.row]
        cell.movie = movieToDisplay
        
        cell.selectionStyle = .none
        cell.title.text! = movieToDisplay.name
        let backgroundQueue = DispatchQueue(label: "com.app.queue",
                                            qos: .default,
                                            target: nil)
        
        backgroundQueue.async {
            let imageData = try? Data(contentsOf: movieToDisplay.poster)
            //            let image = UIImage(data: imageData!)
            //            cell.imageView?.image = image
            
            
            if imageData != nil {
                let image = UIImage(data: imageData!)
                DispatchQueue.main.async(execute: { () -> Void in
                    cell.poster?.image = image
                    cell.setNeedsLayout()
                })
            }
        }
        cell.director.text = "Director:" + movieToDisplay.director
        cell.cast.text = "Cast:" + movieToDisplay.cast
        
        let ratingString: String = String(describing: (movieToDisplay.rating))
        cell.rating.text! = "IMDB rating:" + ratingString

        return cell
    }
    /*
     MARK: - Navigation

     In a storyboard-based application, you will often want to do a little preparation before navigation
    */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        Get the new view controller using segue.destinationViewController.
//        Pass the selected object to the new view controller.
        if segue.identifier == "SignOut" {
            if segue.destination is ViewController {
                if (GIDSignIn.sharedInstance().currentUser) != nil {
                    GIDSignIn.sharedInstance().signOut()
                }
            }
        } else if segue.identifier == "showSingleMovie"{
            
            let destViewController  = segue.destination as! MovieViewController
            
            let tableCell : MovieTableViewCell = sender as! MovieTableViewCell
            let indexPath = table.indexPath(for: tableCell)
            let cellIndex = indexPath?.row
            let requiredInfo = model[cellIndex!]
            destViewController.movieToDisplay = requiredInfo
        } else if segue.identifier == "showSingleFromCollection"{
             let destViewController  = segue.destination as! MovieViewController
             let cell : MovieCollectionViewCell = sender as! MovieCollectionViewCell
             let indexPath = collection.indexPath(for: cell)
             let requiredInfo = model[(indexPath?.row)!]
             destViewController.movieToDisplay = requiredInfo

        }

    }


}
