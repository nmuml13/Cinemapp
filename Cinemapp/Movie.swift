//
//  Movie.swift
//  Cinemapp
//
//  Created by nini on 24/01/17.
//  Copyright © 2017 nini. All rights reserved.
//

import Foundation

struct Movie {
    var name : String
    var director: String
    var cast: String
    var duration: String
    var sessions: Array<Session>
    var rating: Double
    var poster: URL
    var videoID: String
    
    static func == (lhs: Movie, rhs: Movie) -> Bool {
        return lhs.name == rhs.name && lhs.director == rhs.director
    }
}


