//
//  WatchlistTableViewCell.swift
//  Cinemapp
//
//  Created by nini on 28/01/17.
//  Copyright © 2017 nini. All rights reserved.
//

import UIKit

class WatchlistTableViewCell: UITableViewCell {
    var bookedSession : Session?
    var bookedMovie: Movie?
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var seeInfo: UIButton!

  
    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        seeInfo.layer.cornerRadius = 10
        seeInfo.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
